defmodule TheNameGameTest do
  use ExUnit.Case
  doctest TheNameGame

  describe "TheNameGame.verse/1" do
    test "Gary" do
      assert """
             Gary, Gary, bo-bary
             Banana-fana fo-fary
             Fee-fi-mo-mary
             Gary!
             """ == TheNameGame.verse("Gary")
    end

    test "Earl" do
      assert """
             Earl, Earl, bo-bearl
             Banana-fana fo-fearl
             Fee-fi-mo-mearl
             Earl!
             """ == TheNameGame.verse("Earl")
    end

    test "Billy" do
      assert """
             Billy, Billy, bo-illy
             Banana-fana fo-filly
             Fee-fi-mo-milly
             Billy!
             """ == TheNameGame.verse("Billy")
    end

    test "Felix" do
      assert """
             Felix, Felix, bo-belix
             Banana-fana fo-elix
             Fee-fi-mo-melix
             Felix!
             """ == TheNameGame.verse("Felix")
    end

    test "Mary" do
      assert """
             Mary, Mary, bo-bary
             Banana-fana fo-fary
             Fee-fi-mo-ary
             Mary!
             """ == TheNameGame.verse("Mary")
    end

    test "Shirley" do
      assert """
             Shirley, Shirley, bo-bhirley
             Banana-fana fo-fhirley
             Fee-fi-mo-mhirley
             Shirley!
             """ == TheNameGame.verse("Shirley")
    end
  end
end
