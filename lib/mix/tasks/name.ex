defmodule Mix.Tasks.Name do
  use Mix.Task

  @impl Mix.Task
  def run([name | _args]) do
    TheNameGame.verse(name)
    |> Mix.shell().info()
  end
end
