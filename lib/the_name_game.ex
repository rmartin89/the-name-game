defmodule TheNameGame do
  @moduledoc """
  Documentation for `TheNameGame`.
  """

  @spec verse(binary()) :: binary()
  def verse(name) do
    name
    |> String.codepoints()
    |> change()
  end

  @spec change(list(binary())) :: binary()
  defp change(name) do
    """
    #{name}, #{name}, bo-#{change_initial(name, "B")}
    Banana-fana fo-#{change_initial(name, "F")}
    Fee-fi-mo-#{change_initial(name, "M")}
    #{name}!
    """
  end

  defguardp is_vowel(letter) when letter in ["A", "E", "I", "O", "U"]

  defguardp is_offending_initial(letter) when letter in ["B", "F", "M"]

  @spec change_initial(list(binary()), binary()) :: binary()
  defp change_initial([first | _rest] = name, letter) when is_vowel(first),
    do: String.downcase("#{letter}#{name}")

  defp change_initial([first | rest], letter)
       when is_offending_initial(first) and first == letter,
       do: rest

  defp change_initial([_first | rest], letter), do: String.downcase("#{letter}#{rest}")
end
